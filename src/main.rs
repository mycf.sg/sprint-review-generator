#![warn(clippy::all)]
use std::{
    env,
    fs::File,
    io::{Error, ErrorKind, Write},
};

extern crate reqwest;
use reqwest::blocking;

extern crate chrono;
use chrono::FixedOffset;

extern crate dotenv;
use dotenv::dotenv;

extern crate serde;
extern crate serde_json;

mod sprint;
use self::sprint::Sprint;
mod types;
use self::types::{Iteration, Story};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv().expect(
        ".env file not found.\nAdd credentials to a copy of .env-example and save it as .env",
    );
    let token = env::var("PIVOTAL_TOKEN").expect("PIVOTAL_TOKEN not set");
    let emp_project_id =
        env::var("EMP_PIVOTAL_PROJECT_ID").expect("EMP_PIVOTAL_PROJECT_ID not set");
    let emp_stories_url = format!(
        "https://www.pivotaltracker.com/services/v5/projects/{}/iterations?scope=current_backlog&limit=1",
        emp_project_id
    );
    let js_project_id = env::var("JS_PIVOTAL_PROJECT_ID").expect("JS_PIVOTAL_PROJECT_ID not set");
    let js_stories_url = format!(
        "https://www.pivotaltracker.com/services/v5/projects/{}/iterations?scope=current_backlog&limit=1",
        js_project_id
    );
    let sgt_timezone = FixedOffset::east(8 * 3600);

    let current_jobseeker_iteration =
        get_stories_in_current_iteration(&js_stories_url, &token).unwrap();
    let current_employer_iteration =
        get_stories_in_current_iteration(&emp_stories_url, &token).unwrap();
    if !(current_jobseeker_iteration.start == current_employer_iteration.start
        && current_jobseeker_iteration.finish == current_employer_iteration.finish
        && current_jobseeker_iteration.number == current_employer_iteration.number)
    {
        // TODO: this is really ugly, I'm sure there's a prettier way of returning this error
        return Err(Box::new(Error::new(
            ErrorKind::Other,
            "The current iteration for the employer and jobseeker boards must have the same number, start and end date.",
        )));
    }
    let start_time_in_sgt = current_jobseeker_iteration
        .start
        .with_timezone(&sgt_timezone);
    let finish_time_in_sgt = current_jobseeker_iteration
        .finish
        .with_timezone(&sgt_timezone);
    println!(
        "Got stories in sprint {} from {} to {}",
        current_jobseeker_iteration.number, start_time_in_sgt, finish_time_in_sgt
    );

    let jobseeker_stories: Vec<Story> = current_jobseeker_iteration
        .stories
        .into_iter()
        .filter(|story| story.story_type != "release")
        .collect();
    let employer_stories: Vec<Story> = current_employer_iteration
        .stories
        .into_iter()
        .filter(|story| story.story_type != "release")
        .collect();

    let path = "presentation.md"; // TODO: don't hardcode this
    println!("Writing output to presentation.md");
    let mut output = File::create(path)?;
    let sprint = Sprint {
        jobseeker_stories,
        employer_stories,
        other_stories: vec![], // placeholder for when we use a third board for common stories
        start: start_time_in_sgt,
        finish: finish_time_in_sgt,
        number: current_jobseeker_iteration.number,
    };

    write!(output, "{}", sprint.to_markdown_slides())?;
    println!("Markdown source generated. After editing it, use the convert.sh script with Pandoc installed to generate powerpoint slides.");

    Ok(())
}

fn get_stories_in_current_iteration(
    board_url: &str,
    token: &str,
) -> Result<Iteration, Box<dyn std::error::Error>> {
    let client = blocking::Client::new();
    let resp = client
        .get(board_url)
        .header("X-TrackerToken", token)
        .send()?;
    if !resp.status().is_success() {
        return Err(Box::new(Error::new(
            ErrorKind::Other,
            format!("Received status code {} from Pivotal.", resp.status()),
        )));
    }
    let iterations: Vec<Iteration> = resp.json()?;
    let current_iteration: Iteration = iterations
        .get(0)
        .cloned()
        .expect("Could not retrieve details about the current iteration.");
    Ok(current_iteration)
}
