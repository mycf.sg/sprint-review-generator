use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Clone, Deserialize, Debug)]
pub struct Iteration {
    pub finish: DateTime<Utc>,
    pub kind: String,
    pub number: i32,
    pub start: DateTime<Utc>,
    pub stories: Vec<Story>,
}

#[derive(Clone, Deserialize, Debug, PartialEq)]
pub struct Story {
    pub current_state: String,
    pub description: Option<String>,
    pub id: i32,
    pub kind: String,
    pub labels: Vec<Label>,
    pub name: String,
    pub url: String,
    pub story_type: String,
}

impl Story {
    pub fn has_label(&self, label_id: i32) -> bool {
        self.labels.iter().any(|label| label.id == label_id)
    }
}

#[derive(Clone, Deserialize, Debug, PartialEq)]
pub struct Label {
    pub id: i32,
    pub name: String,
}
