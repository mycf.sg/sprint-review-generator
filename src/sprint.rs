use crate::types::Story;
use chrono::{DateTime, FixedOffset};

/// Information about the current sprint / pivotal iteration
pub struct Sprint {
    /// The sprint / iteration number, used in the title slide
    pub number: i32,
    /// The start date of the sprint, used in the title slide
    pub start: DateTime<FixedOffset>,
    /// The end date of the sprint, used in the title slide
    pub finish: DateTime<FixedOffset>,
    /// Stories in the JS / CC4 board
    pub jobseeker_stories: Vec<Story>,
    /// Stories on the emp board
    pub employer_stories: Vec<Story>,
    /// All other stories (this is just a placeholder in case we have a third board)
    pub other_stories: Vec<Story>,
}

impl Sprint {
    pub fn to_markdown_slides(&self) -> String {
        let jobseeker_features: Vec<Story> = self
            .jobseeker_stories
            .iter()
            .cloned()
            .filter(|story| story.story_type == "feature")
            .collect();
        let jobseeker_bugs: Vec<Story> = self
            .jobseeker_stories
            .iter()
            .cloned()
            .filter(|story| story.story_type == "bug")
            .collect();
        let jobseeker_chores: Vec<Story> = self
            .jobseeker_stories
            .iter()
            .cloned()
            .filter(|story| story.story_type == "chore")
            .collect();
        let employer_features: Vec<Story> = self
            .employer_stories
            .iter()
            .cloned()
            .filter(|story| story.story_type == "feature")
            .collect();
        let employer_bugs: Vec<Story> = self
            .employer_stories
            .iter()
            .cloned()
            .filter(|story| story.story_type == "bug")
            .collect();
        let employer_chores: Vec<Story> = self
            .employer_stories
            .iter()
            .cloned()
            .filter(|story| story.story_type == "chore")
            .collect();
        let mut slides: Vec<String> = vec![
            self.generate_title_slide(),
            self.generate_done_stories_slide(),
            generate_slides_for_stories(
                "## Jobseeker - In Progress Features",
                &jobseeker_features,
                false,
            ),
            generate_slides_for_stories("## Jobseeker - In Progress Bugs", &jobseeker_bugs, false),
            generate_slides_for_stories(
                "## Jobseeker - In Progress Chores",
                &jobseeker_chores,
                false,
            ),
            generate_slides_for_stories(
                "## Employer - In Progress Features",
                &employer_features,
                false,
            ),
            generate_slides_for_stories("## Employer - In Progress Bugs", &employer_bugs, false),
            generate_slides_for_stories(
                "## Employer - In Progress Chores",
                &employer_chores,
                false,
            ),
        ];
        if self
            .other_stories
            .iter()
            .any(|story| story.current_state != "accepted")
        {
            slides.push(generate_slides_for_stories(
                "## Other - In Progress",
                &self.other_stories,
                false,
            ));
        }
        slides.join("\n\n")
    }

    fn generate_title_slide(&self) -> String {
        format!(
            "---\ntitle: Sprint {}\ndate: {} - {}\n---",
            self.number,
            self.start.format("%d %b"),
            self.finish.format("%d %b")
        )
    }

    fn generate_done_stories_slide(&self) -> String {
        let jobseeker_stories =
            generate_markdown_for_stories_with_state(&self.jobseeker_stories, "accepted");
        let employer_stories =
            generate_markdown_for_stories_with_state(&self.employer_stories, "accepted");
        let other_stories =
            generate_markdown_for_stories_with_state(&self.other_stories, "accepted");

        let mut slide: Vec<String> = vec![];
        if !jobseeker_stories.is_empty() {
            slide.push(format!("*Jobseeker*\n\n{}", jobseeker_stories.join("\n")));
        }
        if !employer_stories.is_empty() {
            slide.push(format!("*Employer*\n\n{}", employer_stories.join("\n")));
        }
        if !other_stories.is_empty() {
            slide.push(format!("*Other*\n\n{}", other_stories.join("\n")));
        }
        format!("## Sprint backlog - Done!\n\n{}", slide.join("\n\n"))
    }
}

/// Generates slides from the given list of stories
///
/// Stories are rendered into 6 separate lists
/// If the `split_across_2_slides` parameter is true, then the first slide contains rejected, delivered and finished stories, while the second contains started, unstarted and planned stories.
/// Accepted stories are ignored
fn generate_slides_for_stories(
    title: &str,
    stories: &[Story],
    split_across_2_slides: bool,
) -> String {
    let rejected_stories = generate_markdown_for_stories_with_state(stories, "rejected");
    let delivered_stories = generate_markdown_for_stories_with_state(stories, "delivered");
    let finished_stories = generate_markdown_for_stories_with_state(stories, "finished");
    let started_stories = generate_markdown_for_stories_with_state(stories, "started");
    let unstarted_stories = generate_markdown_for_stories_with_state(stories, "unstarted");
    let planned_stories = generate_markdown_for_stories_with_state(stories, "planned");

    let mut slide1: Vec<String> = Vec::new();
    if !rejected_stories.is_empty() {
        slide1.push(format!("*Rejected*\n\n{}", rejected_stories.join("\n")));
    }
    if !delivered_stories.is_empty() {
        slide1.push(format!("*Delivered*\n\n{}", delivered_stories.join("\n")));
    }
    if !finished_stories.is_empty() {
        slide1.push(format!("*Finished*\n\n{}", finished_stories.join("\n")));
    }
    let mut slide2: Vec<String> = Vec::new();
    if !started_stories.is_empty() {
        slide2.push(format!("*Started*\n\n{}", started_stories.join("\n")));
    }
    if !unstarted_stories.is_empty() {
        slide2.push(format!("*Unstarted*\n\n{}", unstarted_stories.join("\n")));
    }
    if !planned_stories.is_empty() {
        slide2.push(format!("*Planned*\n\n{}", planned_stories.join("\n")));
    }

    if split_across_2_slides {
        format!(
            "{}\n\n{}\n\n{} 2\n\n{}",
            title,
            slide1.join("\n\n"),
            title,
            slide2.join("\n\n")
        )
    } else {
        let mut slide = slide1;
        slide.extend(slide2);
        format!("{}\n\n{}", title, slide.join("\n\n"))
    }
}

fn generate_markdown_for_stories_with_state(stories: &[Story], current_state: &str) -> Vec<String> {
    stories
        .iter()
        .filter(|story| story.current_state == current_state)
        .map(|story| format!("* {}", story.name))
        .collect()
}
