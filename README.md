# Mcf-Sprint-Review-Generator: generates slides for sprint review using the Pivotal Tracker API

## Usage

1. Create a copy of the `.env-example` file as `.env`, and follow the instructions in the file to set the required environment variables.
2. Install [Pandoc](https://pandoc.org/), which is able to convert markdown into powerpoint slides.
3. With docker installed, do `docker-compose pull` and `docker-compose up`
    
    This retrieves stories from the current iteration, and generates markdown. You will probably want to inspect the file, and edit as needed.
4. Use the included `convert.sh` script, which invokes Pandoc to generate `presentation.pptx`.

## Building From Source for Development

This requires a local installation of the Rust toolchain.

As usual for Cargo, build with `cargo build` and run with `cargo run`.

## License

MIT
